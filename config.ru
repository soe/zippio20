require 'yaml'
require 'json'

require 'iron_worker'
require 'metaforce'
require 'pubnub'

require 'sinatra'
require 'redis'

# configure IronWorker
IronWorker.configure do |config|
  config.token = ENV['IRON_WORKER_TOKEN']
  config.project_id = ENV['IRON_WORKER_PROJECT_ID']
end

# sessions
enable :sessions

# call the Sinatra app - "app.rb"
require './app'
run Sinatra::Application
