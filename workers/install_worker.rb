
class InstallWorker < IronWorker::Base

  merge_gem 'mechanize'
  merge_gem 'pubnub'
  merge_gem 'redis'

  attr_accessor :queue_name, :token, :project_id, :package_id, :continue, :config

  def run

    IronWorker.logger.info "package id: #{package_id}"
    IronWorker.logger.info "continue?: #{continue}"

    # initiate Pubsub
    pubnub = Pubnub.new(config['PUBNUB_PUBLISH_KEY'], config['PUBNUB_SUBSCRIBE_KEY'], config['PUBNUB_SECRET_KEY'], '', false)

    # initiate Redis
    uri = URI.parse(config["REDISTOGO_URL"])
    redis = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)

    # initiate Mechanize
    agent = Mechanize.new

    require 'uri'

    salesforce_uri = config['SFDC_URI']

    # status update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'status','text' => 'Starting installation...'}
    })

    # login page
    p0 = agent.get(salesforce_uri)
    IronWorker.logger.info "p0: "+ p0.title

    # fill up the form
    f = p0.form("login")
    f.un = config['SFDC_USERNAME']
    f.pw = config['SFDC_PASSWORD']

    # log in by submitting the form
    # progress update - 30%
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'progress','percent' => '20','text' => 'Logging in... 20%'}
    })
    p1 = agent.submit(f)
    
    p2 = agent.get(salesforce_uri + "/packaging/installPackage.apexp?p0=" + package_id)

    if p2.form("login") # login not successful
      # send warning message - login failed
      pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
        'message' => {'type' => 'warning','text' => 'Salesforce login failed, please inform the admin.'}
      })
      # reset package status
      redis.del "p:#{package_id}:status"

    else # login successful

      package_name = p2.search("//*[@id='InstallPackagePage:InstallPackageForm:j_id18:j_id19:j_id20:NameText']").text
      
      _package_name = URI.escape(package_name) # url safe

      if not package_name # package does not exist
        # send warning message - package does not exist
        pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
          'message' => {'type' => 'warning','text' => 'The package does not exist. Please check the package ID.'}
        })
        # reset package status
        redis.del "p:#{package_id}:status"

      else # package is available
        IronWorker.logger.info "p2: "+ package_name
        # progress update - 50%
        pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
          'message' => {'type' => 'progress','percent' => '40','text' => 'Getting package info... 40%'}
        })

        package_version = p2.search("//*[@id='InstallPackagePage:InstallPackageForm:j_id18:j_id19:j_id22:VersionText']").text
        package_publisher = p2.search("//*[@id='InstallPackagePage:InstallPackageForm:j_id18:j_id19:j_id23:PublisherText']").text
        package_description = p2.search("//*[@id='InstallPackagePage:InstallPackageForm:j_id18:j_id19:j_id24:DescriptionText']").text
        
        # send package info
        pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
          'message' => {'type' => 'package','name' => package_name,'version' => package_version,'publisher' => package_publisher,'description' => package_description}
        })
        
        # Step 1. Approve Package API access
        # also approve API access accordingly
        p3 = agent.get(salesforce_uri + "/_ui/core/mfpackage/install/PackageImportStageManager?p0=" + package_id+ "&packageName=" + package_name)
        IronWorker.logger.info "p3: "+ p3.title

        # progress update - 70%
        pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
          'message' => {'type' => 'progress','percent' => '60','text' => 'Installation step 1... 60%'}
        })

        # look for error message
        error = p3.search("//*[@class='pbWizardBody']")

        # can't install cos' already installed
        if error.text.index("already installed")
          # status update
          pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
            'message' => {'type' => 'status','text' => 'Already installed...'}
          })

          # update package's status and info
          redis.set "p:#{package_id}:status", "installed"
          redis.set "p:#{package_id}:name", package_name
          redis.set "p:#{package_id}:version", package_version
          redis.set "p:#{package_id}:publisher", package_publisher
          redis.set "p:#{package_id}:description", package_description

          # call retrieve worker
          if continue
            r = agent.get("http://zippio.herokuapp.com/worker/retrieve/?id=#{package_id}&name=#{_package_name}")
            IronWorker.logger.info "call retrieve worker, task id: #{r.content}"
          end

          IronWorker.logger.info 'Already installed...'
          exit 0 # successful exit, since it is already installed

        # can't install for other reasons
        else 

          error_list = error.search("//table[@class='list']")

          if error_list.text.delete(' ').length > 0

            IronWorker.logger.info error_list.inner_html

            # send warning message - about error
            pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
              'message' => {'type' => 'warning','text' => "<table>#{error_list.inner_html}</table>"}
            })

          end

        end # already installed?

        f = p3.form("stageForm")

        # Step 2. Install Package
        # submit the form via "Next" button
        p4 = agent.submit(f, f.buttons[0])

        if not p4.title # step 1 - not ok

          IronWorker.logger.info "p4 - error: "+ p4.content

          # send warning message - installation stopped at step 1
          pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
            'message' => {'type' => 'warning','text' => 'Package installation failed at Step 1.'}
          })
          # reset package status
          redis.del "p:#{package_id}:status"

        else # step 1 - ok
          IronWorker.logger.info "p4: "+ p4.title

          # progress update - 90%
          pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
            'message' => {'type' => 'progress','percent' => '80','text' => 'Installation step 2... 80%'}
          })

          f = p4.form("stageForm")

          # Step 3. Installion
          # submit the form via "Install" button
          p5 = agent.submit(f, f.buttons[1])

          if not p5.title # step 2 - not ok

            IronWorker.logger.info "p5 - error: "+ p5.content

            # send warning message - installation stopped at step 2
            pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
              'message' => {'type' => 'warning','text' => 'Package installation failed at Step 2.'}
            })
            # reset package status
            redis.del "p:#{package_id}:status"
          else # step 2 - ok
            IronWorker.logger.info "p5: "+ p5.title

            # progress update - 100%
            pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
              'message' => {'type' => 'progress','percent' => '100','text' => 'Installation step 3... 100%'}
            })

            # status update
            pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
              'message' => {'type' => 'status','text' => 'Installation complete...'}
            })

            # update package's status and info
            redis.set "p:#{package_id}:status", "installed"
            redis.set "p:#{package_id}:name", package_name
            redis.set "p:#{package_id}:version", package_version
            redis.set "p:#{package_id}:publisher", package_publisher
            redis.set "p:#{package_id}:description", package_description

            # call retrieve worker
            if continue
              r = agent.get("http://zippio.herokuapp.com/worker/retrieve/?id=#{package_id}&name=#{_package_name}")
              IronWorker.logger.info "call retrieve worker, task id: #{r.content}"
            end

          end # if-else: step 2?
        end # if-else: step 1?
      end # if-else: does package exist?
    end # if-else: is login successful?

  end # def:run

end # class:InstallWorker