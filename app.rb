require 'sinatra'
require 'redis'

require 'uri'
require 'net/https'
require 'json'

require 'logger'

require_relative 'workers/install_worker'
require_relative 'workers/retrieve_worker'
# require_relative 'workers/uninstall_worker'

set :public_folder, File.dirname(__FILE__) + '/static'

configure do
  # initialize REDIS
  uri = URI.parse(ENV["REDISTOGO_URL"] || "redis://redistogo:7403e663956ffd9d666778836babfdfe@panga.redistogo.com:9799/")
  REDIS = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
  
  SFDC_CONSUMER_KEY = ENV["SFDC_CONSUMER_KEY"] || '3MVG98XJQQAccJQfKvGaEmJiaUEcDSEPpaAPj733BTPAuoeG.ONz20zeoqhwi5feVex7uiVZ85SgYBAX5aRFN'
  SFDC_CONSUMER_SECRET = ENV["SFDC_CONSUMER_SECRET"] || '9066990553838573434'
end

################################
# routers
#
# / - home page
#
# /package/?id=<package_id> - package page
# /package/download/?id=<package_id> - package download page
# /package/remove/?id=<package_id> - package remove page
#
# /worker/install/?id=<package_id> - worker install
# /worder/retrieve/?id=<package_id> - worker retrieve 
################################

# main page with info and package link form
get '/' do
  erb :index
end

# login - redirect to Salesforce for authorization
get '/login' do
  redirect_uri = "https://#{request.env['SERVER_NAME']}/callback"
  
  # redirect to Salesforce
  redirect "https://login.salesforce.com/services/oauth2/authorize?response_type=code"\
              "&client_id=#{SFDC_CONSUMER_KEY}"\
              "&client_secret=#{SFDC_CONSUMER_SECRET}"\
              "&redirect_uri=#{URI.escape("#{redirect_uri}")}"
  
end

# receive oauth2 callback from Salesforce
get '/callback' do
  # get code which Salesforce passes
  @code = params[:code]
  
  redirect_uri = "https://#{request.env['SERVER_NAME']}/callback"
  
  # build uri
  uri = URI.parse('https://login.salesforce.com/services/oauth2/token')
  
  # initialize http
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_PEER 
  http.ca_file = '/usr/lib/ssl/certs/ca-certificates.crt'
  
  # build request
  request = Net::HTTP::Post.new(uri.request_uri)
  request.body = "grant_type=authorization_code"\
                    "&code=#{params[:code]}"\
                    "&client_id=#{SFDC_CONSUMER_KEY}"\
                    "&client_secret=#{SFDC_CONSUMER_SECRET}"\
                    "&redirect_uri=#{URI.escape("#{redirect_uri}")}"\
                    "&format=json"
  
  # do request and get response
  response = http.request(request)
  
  # get access token
  logger.info response.body
  res = JSON.parse(response.body)
  
  if res['access_token'] # login success
    session['access_token'] = res['access_token']
    
    redirect '/'
  else # login fail
    session['access_token'] = nil
    
    erb :login_fail
  end
  
end 

# logout - revoke Salesforce access token
get '/logout' do
  # unset sessions
  session['access_token'] = nil
  
  redirect '/'
end

# package page
# if package status is not "retrieved", this page initiates background workers
# if package status is "retrieved", this page displays download link
get '/package/' do

  @package_id = params[:id]
  @reinstall = params[:reinstall]

  @package_name = REDIS.get "p:#{params[:id]}:name"
  @package_version = REDIS.get "p:#{params[:id]}:version"
  @package_publisher = REDIS.get "p:#{params[:id]}:publisher"
  @package_description = REDIS.get "p:#{params[:id]}:description"

  # check if the package is already retreived
  # @status -> nil, installing", "installed", retrieving, "retrieved"
  @status = REDIS.get "p:#{params[:id]}:status"

  # force reinstall
  if @reinstall
    @status = nil
  end

  if @status == "retrieved"
    @package_file = REDIS.get "p:#{params[:id]}:file"
  end 

  # @status is nil -> so call install worker
  if @status == nil
    
    # call install worker - also continue to call retrieve worker
    worker = call_install_worker(params[:id], true)

    @task_id = worker.task_id
  end

  # @status is nil -> so call retrieve worker
  if @status == "installed"
    # call retrieve worker
    worker = call_retrieve_worker(params[:id])

    @task_id = worker.task_id
  end

  # for PUBNUB javascript to work
  @pub_key = ENV['PUBNUB_PUBLISH_KEY']
  @sub_key = ENV['PUBNUB_SUBSCRIBE_KEY']

  erb :package
end

# package download page
# get resource_id from redis then stream the file from Google Drive
get '/package/download/' do

  # get resource_id from redis - stored by RetrieveWorker
  @resource_id = REDIS.get "p:#{params[:id]}:file"

  # initiate GoogleDrive
  drive = GoogleDrive.login(ENV["GDRIVE_USERNAME"], ENV["GDRIVE_PASSWORD"])

  # search by title
  files = drive.files("title" => "sf-zippio-#{params[:id]}.zip", "title-exact" => true)

  # loop through files with same title to match against the resource_id
  for f in files
    if @resource_id == f.resource_id

      # stream the file download from Google Drive
      response.headers['content_type'] = "application/octet-stream"
      attachment("sf-zippio-#{params[:id]}.zip")
      response.write(f.download_to_string())

      # stop looping, we go the right one already
      break

    end # if
  end # for
end

# package remove page
# currently for testing purpose only
get '/package/remove/' do
  # this reset the package status, for quick testing
  REDIS.del "p:#{params[:id]}:status"

  # maybe uninstall the package from Salesforce too?

  params[:id]
end

# iron.io/worker -> install_worker
# an url enpoint for call_install_worker method
get '/worker/install/' do

  worker = call_install_worker(params[:id])

  worker.task_id

  #worker.wait_until_complete
  #p worker.status
end

# iron.io/worker -> retrieve_worker
# an url enpoint for call_retrieve_worker method
get '/worker/retrieve/' do

  worker = call_retrieve_worker(params[:id], params[:name])

  worker.task_id

  #worker.wait_until_complete
  #p worker.status
end

# for all other pages, redirect to home page
get '*' do
  redirect '/'
end

################################
# methods
################################

# add to InstallWorker queue in "IronWorker"
def call_install_worker(id, continue = false)

  worker = InstallWorker.new

  worker.token = IronWorker.config.token
  worker.project_id = IronWorker.config.project_id
  worker.queue_name = "install"
  worker.package_id = id
  worker.continue = continue

  worker.config = {
    "PUBNUB_PUBLISH_KEY" => ENV['PUBNUB_PUBLISH_KEY'],
    "PUBNUB_SUBSCRIBE_KEY" => ENV['PUBNUB_SUBSCRIBE_KEY'],
    "PUBNUB_SECRET_KEY" => ENV['PUBNUB_SECRET_KEY'],

    "SFDC_USERNAME" => ENV['SFDC_USERNAME'],
    "SFDC_PASSWORD" => ENV['SFDC_PASSWORD'],
    "SFDC_URI" => ENV['SFDC_URI'],

    "REDISTOGO_URL" => ENV['REDISTOGO_URL'],
  }

  # update status
  REDIS.set "p:#{id}:status", "installing"
  # set it to expire, then the status is set to nil
  # if the process is stuck, then it become reset
  REDIS.expire "p:#{id}:status", 180

  # add worker to the queue
  worker.queue

  return worker
end

# add to RetrieveWorker queue in "IronWorker"
def call_retrieve_worker(id, name = nil)

  worker = RetrieveWorker.new

  worker.queue_name = "retrieve"
  worker.package_id = id
  worker.package_name = name || REDIS.get("p:#{id}:name")

  worker.token = IronWorker.config.token
  worker.project_id = IronWorker.config.project_id

  worker.config = {
    "PUBNUB_PUBLISH_KEY" => ENV['PUBNUB_PUBLISH_KEY'],
    "PUBNUB_SUBSCRIBE_KEY" => ENV['PUBNUB_SUBSCRIBE_KEY'],
    "PUBNUB_SECRET_KEY" => ENV['PUBNUB_SECRET_KEY'],

    "SFDC_USERNAME" => ENV['SFDC_USERNAME'],
    "SFDC_PASSWORD" => ENV['SFDC_PASSWORD'],
    "SFDC_SECURITY_TOKEN" => ENV['SFDC_SECURITY_TOKEN'],

    "REDISTOGO_URL" => ENV['REDISTOGO_URL'],

    "GDRIVE_USERNAME" => ENV['GDRIVE_USERNAME'],
    "GDRIVE_PASSWORD" => ENV['GDRIVE_PASSWORD'],
  }
  
  # update status
  REDIS.set "p:#{id}:status", "retrieving"
  # set it to expire, then the status is set to nil
  # if the process is stuck, then it become reset
  REDIS.expire "p:#{id}:status", 180

  # add worker to the queue
  worker.queue

  return worker
end
